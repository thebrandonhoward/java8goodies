package com.example.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;

import java.util.function.Function;
import java.util.stream.Stream;

public class Functional_Validations {

    public static void main(String[] args) {

        Automobile automobile = new Automobile("Dodge", "Ram", 2021);
        Automobile automobile1 = new Automobile(null, "Ram", 2021);
        Automobile automobile2 = new Automobile("Dodge", "", 2021);
        Automobile automobile3 = new Automobile("Dodge", "Ram", 3021);
        Automobile automobile4 = new Automobile("Dodge", "Ram", 2021);
        Automobile automobile5 = new Automobile("Dodge", "Ram", 2021);

        //You can also comment these and use the functions outside of the method
        Function<Automobile,Automobile> validNull = a -> a != null ? a : null;
        Function<Automobile,Automobile> validMake = a -> (a != null && a.getMake() != null && !a.getMake().isEmpty()) ? a : null;
        Function<Automobile,Automobile> validModel = a -> (a != null && a.getModel() != null && !a.getModel().isEmpty()) ? a : null;
        Function<Automobile,Automobile> validYear = a -> (a != null && a.getYear() >= 1500 && a.getYear() <= 2023) ? a : null;

        Function<Automobile, Automobile> automobileVFunction
                = validNull.andThen(validMake).andThen(validModel).andThen(validYear);

        Stream.of( automobileVFunction.apply(automobile)
                  ,automobileVFunction.apply(automobile1)
                  ,automobileVFunction.apply(automobile2)
                  ,automobileVFunction.apply(automobile3)
                  ,automobileVFunction.apply(automobile4)
                  ,automobileVFunction.apply(automobile5) )
              .forEach(System.out::println);

    }

    // These functions will work outside of the method also. See the below examples:
    //static Function<Automobile,Automobile> validNull = automobile -> automobile != null ? automobile : null;
    //static Function<Automobile,Automobile> validMake = automobile -> (automobile != null && automobile.getMake() != null && !automobile.getMake().isEmpty()) ? automobile : null;
    //static Function<Automobile,Automobile> validModel = automobile -> (automobile != null && automobile.getModel() != null && !automobile.getModel().isEmpty()) ? automobile : null;
    //static Function<Automobile,Automobile> validYear = automobile -> (automobile != null && automobile.getYear() >= 1500 && automobile.getYear() <= 2023) ? automobile : null;

    @Data
    @AllArgsConstructor
    static class Automobile {
        private String make;
        private String model;
        private int year;

        @Override
        @SneakyThrows
        public String toString() {
                return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        }
    }

    static Function<Automobile, Automobile> validNull = automobile -> automobile != null ? automobile : null;
    static Function<Automobile, Automobile> validMake = automobile -> (automobile != null && automobile.getMake() != null && !automobile.getMake().isEmpty()) ? automobile : null;
    static Function<Automobile, Automobile> validModel = automobile -> (automobile != null && automobile.getModel() != null && !automobile.getModel().isEmpty()) ? automobile : null;
    static Function<Automobile, Automobile> validYear = automobile -> (automobile != null && automobile.getYear() >= 1500 && automobile.getYear() <= 2023) ? automobile : null;

}
