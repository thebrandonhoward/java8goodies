package com.example.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import java.util.function.Function;
import java.util.stream.Stream;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	public static void main(String[] args) {

		Automobile automobile = new Automobile("Dodge", "Ram", 2021);
		Automobile automobile1 = new Automobile(null, "Ram", 2021);
		Automobile automobile2 = new Automobile("Dodge", "", 2021);
		Automobile automobile3 = new Automobile("Dodge", "Ram", 3021);
		Automobile automobile4 = new Automobile("Dodge", "Ram", 2021);
		Automobile automobile5 = new Automobile("Dodge", "Ram", 2021);

		Function<Automobile,Automobile> automobileVFunction
				= validNull.andThen(validMake).andThen(validModel).andThen(validYear);

		Stream.of(automobileVFunction.apply(automobile)
		          ,automobileVFunction.apply(automobile1)
		          ,automobileVFunction.apply(automobile2)
		          ,automobileVFunction.apply(automobile3)
		          ,automobileVFunction.apply(automobile4)
		          ,automobileVFunction.apply(automobile5))
				.forEach(System.out::println);


		//SpringApplication.run(DemoApplication.class, args);

	}

	@Data
	//@AllArgsConstructor
	static class Automobile {
		private String make;
		private String model;
		private int year;

		public Automobile(String make, String model, int year) {
			this.make = make;
			this.model = model;
			this.year = year;
		}

		public String getMake() {
			return make;
		}

		public void setMake(String make) {
			this.make = make;
		}

		public String getModel() {
			return model;
		}

		public void setModel(String model) {
			this.model = model;
		}

		public int getYear() {
			return year;
		}

		public void setYear(int year) {
			this.year = year;
		}

		@Override
		@SneakyThrows
		public String toString() {
			try {
				return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
			}
			catch(Exception e) {
				return "";
			}
		}
	}

	static Function<Automobile,Automobile> validNull = automobile -> automobile != null ? automobile : null;
	static Function<Automobile,Automobile> validMake = automobile -> (automobile != null && automobile.getMake() != null && !automobile.getMake().isEmpty()) ? automobile : null;
	static Function<Automobile,Automobile> validModel = automobile -> (automobile != null && automobile.getModel() != null && !automobile.getModel().isEmpty()) ? automobile : null;
	static Function<Automobile,Automobile> validYear = automobile -> (automobile != null && automobile.getYear() >= 1500 && automobile.getYear() <= 2023) ? automobile : null;

}