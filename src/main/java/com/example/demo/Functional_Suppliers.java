package com.example.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;

import java.util.Random;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Functional_Suppliers {
    public static void main(String[] args) {

        startLog.accept("Functional_Predicates ");

        Automobile automobile = new Automobile(randomId.get(), "Dodge", "Ram", 2021);
        Automobile automobile1 = new Automobile(randomId.get(),null, "Ram", 2021);
        Automobile automobile2 = new Automobile(randomId.get(),"Dodge", "", 2021);
        Automobile automobile3 = new Automobile(randomId.get(),"Dodge", "Ram", 3021);
        Automobile automobile4 = new Automobile(randomId.get(),"Dodge", "Ram", 2021);
        Automobile automobile5 = new Automobile(randomId.get(),"Dodge", "Ram", 2021);

        Stream.of(automobile, automobile1, automobile2, automobile3, automobile4, automobile5)
                .forEach(System.out::println);

        Stream.of(automobile, automobile1, automobile2, automobile3, automobile4, automobile5)
                .forEach( autos -> System.out.printf( "IS OBJECT VALID: %b\n", validNull.and(validMake).and(validModel).and(validYear).test(autos) ) );

        endLog.accept("Functional", "_Predicates");
    }

    static Consumer<String> startLog = msg -> System.out.println(msg + " Starting...");
    static BiConsumer<String, String> endLog = (msg1, msg2) -> { System.out.println(msg1 + msg2 + " Complete"); };

    static Supplier<Integer> randomId = () -> Math.abs(new Random().nextInt());

    @Data
    @AllArgsConstructor
    static class Automobile {
        private int id;
        private String make;
        private String model;
        private int year;

        @Override
        @SneakyThrows
        public String toString() {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        }
    }

    static Predicate<Automobile> validNull = automobile -> automobile != null;
    static Predicate<Automobile> validMake = automobile -> automobile.getMake() != null && !automobile.getMake().isEmpty();
    static Predicate<Automobile> validModel = automobile -> automobile.getModel() != null && !automobile.getModel().isEmpty();
    static Predicate<Automobile> validYear = automobile -> automobile.getYear() >= 1500 && automobile.getYear() <= 2023;

}
